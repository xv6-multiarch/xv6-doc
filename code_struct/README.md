# Code structure

Decisions made about the structure of code and the reasons behind them.

## Main structure

How do we organize the source files?

### Simplicity over structure

The XV6 Multi Architecture project relies primarily on MIT [Parallel & Distributed Operating Systems Group](https://pdos.csail.mit.edu/) and the operating system they use for their [6.828: Operating System Engineering](https://pdos.csail.mit.edu/6.828] course: [XV6](https://github.com/mit-pdos/xv6-public).

Their source code is targeting i386 architecture, for teaching purposes, and is a flat set of files within an unique directory, for the same simplicity.

### Multiple ports imply dedicated folders

Most ports of XV6 on other architectures made different choices, structuring code and build object within dedicated folders, in order to avoid the large amount of files when editing, compiling, linking and making file system within the same folder.

As we strongly support the goal of teaching and the corresponding need for simplicity and clarity, we remain dedicated to avoid unnecessary scissoring and splitting of the source code. But, as each processor demands some assembly code with its own syntax, we had to put it in distinct folder (one for each instruction set implementation).

### User vs Kernel code

As we created the 'arch' folder for the different ports, we though about the rest of the source files, and the best structure for them.

Once we split some of the code within a few folders (for technical imperatives), we could consider splitting the remaining code within appropriate structure.

All classes on Unix-like operating systems emphasize the distinction between kernel space and user space programs. So we consider it would be a good idea to put user programs into a dedicated folder, named `usr` after the corresponding directory on Unix systems like Linux.

The corresponding executable files may be put in a `bin` sub-directory as it is in real unices.

The kernel being the central reason of the whole project, we choose to let its remaining source files in the root directory.

### Tools folder?

Most authors of others ports had put the `mkfs` files in a dedicated `tools` sub-directory.

We first considered doing the same in XV6 Multi Arch project, but, as the files are few ones and there is other building stuff which cannot be put in specific folder, we choose to let `mkfs` stuff within the root directory.

## Where to build?

One thing the authors of ports have done which seems interesting for clarity is to generate object files within dedicated folder(s).

### General considerations

The original XV6 project generates all files in the root directory where they mix up with source code and other stuff, multiplying files where the source files are and making difficult to browse them for studying and modifying.

So we think it would be easier for students to have their source code remaining clean whatever they do for building the kernel.

It is of course a questionable point of view, as this choice implies that the building process is harder to understand, mainly because the Makefile is more complex than it was initially). We will try to make it clearer by simplifying the Makefile (which was three times bigger in the original than in most ports).

### A build folder

Most development projects had used two folders for their job:
* `src`: place to put their source files and Makefile
* `build`: place where the generated and temporary files go when the application is built (some call it `binaries`)

As we have no folder dedicated to source files (they are at the root or the project), we have to create the build folder within the project (and to delete it when we clean up).

The final objects (`kernel.elf` and `xv6.img`) will be put in the root directory, as they are the goal which all the code is for.
