# Documentation for XV6 Multi Architecture project

We have felt the need to document some parts of the project and to display the corresponding notes within a dedicated project, in order to let the source code of XV6 clean and clear for teaching purposes.

The documentation is organized as subjects stored in corresponding folders:

* [code_struc](code_struct/README.md): decisions made about the structure of code and the reasons behind them.
* [architectures](architectures/README.md): technical informations and notices about implementations on different hardware (processors, platforms, devices).


