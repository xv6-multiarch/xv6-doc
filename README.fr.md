# Documentation du projet XV6 Multi Architecture

Nous avons ressenti le besoin de documenter certains éléments du projet tout en préservant la clarté du code source de XV6 pour son utilisation en enseignement des systèmes d'exploitation.

Cette documentation est organisée par sujets, chacun dans son propre répertoire :

* [code_struc](code_struct/README.fr.md) : décisions et choix faits en matière d'organisation du code.
* [architectures](architectures/README.fr.md) : informations techniques sur l'implémentation de XV6 sur différents processeurs, plateformes et périphériques.
